# RebornOS installer cnchi Gnome based

![License](https://img.shields.io/github/license/antergos/cnchi.svg)

The latest stable version is: RebornOS-2020.02.08-x86_64.iso

<p align="center">
<img src="https://i.imgur.com/0drTqzc.png">
</p>

<!-- ![Deepin_Image](/images/deepin.png) -->

## Download Locations ##

From the web page of <a href="https://rebornos.org/" class="button">RebornOS</a>

From our download page on <a href="https://osdn.net/projects/rebornos/releases" class="button">OSDN</a>

From our download page on <a href="https://sourceforge.net/projects/rebornos/files/" class="button">Sourceforge</a>

From our <a href="https://repo.rebornos.org/RebornOS/iso/" class="button">Server</a>

<br>

# To Manually Build Yourself

### Dependencies
- isolinux/syslinux
- arch-install-scripts
- cpio
- dosfstools
- git 
- libisoburn
- mkinitcpio-nfs-utils
- make
- opendesktop-fonts
- patch
- squashfs-tools
- archiso
- lynx
- wget
- yad

<br>

### Free space

Please check that you have 5GB (or more) of free harddisk space in your root partition:
`df -h /`

<br>

### Instructions

1. Install dependencies:
```
sudo pacman -S arch-install-scripts cpio dosfstools git libisoburn mkinitcpio-nfs-utils make patch squashfs-tools wget lynx archiso yad --noconfirm --needed
```
2. Clone the repository recursively and `cd` into it (a folder named `cnchi-gnome-based` will be created):
```
git clone https://gitlab.com/reborn-os-team/cnchi-gnome-based.git --recursive && cd cnchi-gnome-based
```
3. Create an `out` folder by running:
```
sudo mkdir out
```
4. Begin building it:
```
sudo ./build.sh -v
```

<br>

### That's it!

To rebuild the ISO, simply remove the `build` folder in addition to emptying the `out` folder. Next, re-enter the command from step 4.

<br>

### General Notes and Information

- `build.sh`: script used to build the ISO. 
- **`HELP_ME.sh`: script to easily build and update an ISO easily. No advance knowledge needed!**
- `packages.both`: simply a list of all packages to be installed in an ISO during the build process. To change the DE, login manager, or any other grouping of packages, simply edit this file.
- `pacman.conf`: the pacman.conf file used by build.sh to build the ISO. With this, it can ignore whatever personal configs you have on your own system in `/etc/pacman.conf` and instead focus solely on this one.
- `run.sh`: um... useless file. Going to get rid of it soon.
- `TEST_FILE.sh`: guess what? Another outdated, useless file waiting to be gotten rid of.
- `translations.sh`: translation magic!
- `pacman-init.service`: adds a pacman module to systemd. I think it works in our ISO, but honestly its something snagged from the Antergos ISO - and I don't entirely know if it does anything in ours. However, it sure does sound like a useful file, right?
- `clean.sh`: easy file to use to remove your `build` and `out` folder, in general just an easy method of getting everything all sparkly clean and ready to build another ISO.
- `config`: configurations for what version of Cnchi to use in our ISO, as well as where to fetch it from.
- `gitv2.sh`: a secial file made by Palanthis so as to automate pushing dev changes up to Gitlab here.

<br>

### How Delete Directory

Once the installation ISO is generated, and copied elsewhere, the directory used for its creation can be deleted by:

```
sudo rm -r cnchi-gnome-based
```

You can access the RebornOS page by <a href="https://rebornos.org/" class="button">clicking here</a>.

<br>
<br>
<br>
<br>

# Cómo construir la ISO del instalador de RebornOS

La última versión estable es: RebornOS-2020.02.08-x86_64.iso

## Sitios desde donde descargar el instalador: ##

Desde la página web de <a href="https://rebornos.org/" class="button">RebornOS</a>

Desde nuestra página de descargas en <a href="https://osdn.net/projects/rebornos/releases" class="button">OSDN</a>

Desde nuestra página de descargas en <a href="https://sourceforge.net/projects/rebornos/files/" class="button">Sourceforge</a>

Desde nuestro <a href="https://repo.rebornos.org/RebornOS/iso/" class="button">Servidor</a>

<br>

### Dependencias
- isolinux/syslinux
- arch-install-scripts
- cpio
- dosfstools
- git 
- libisoburn
- mkinitcpio-nfs-utils
- make
- opendesktop-fonts
- patch
- squashfs-tools
- archiso
- lynx
- wget
- yad

<br>

### Pasos a llevar a cabo para crear el instalador

1. Primero, deberemos de instalar las dependencias:

```
sudo pacman -S arch-install-scripts cpio dosfstools git libisoburn mkinitcpio-nfs-utils make patch squashfs-tools wget lynx archiso yad --noconfirm --needed
```
Una vez instaladas, reiniciamos el sistema.

2. Luego, clonamos el repositorio localmente, y entramos a él (se creará una carpeta de nombre `cnchi-gnome-based`). Esto se hace con:

```
git clone https://gitlab.com/reborn-os-team/cnchi-gnome-based.git --recursive && cd cnchi-gnome-based
```

3. Creamos una carpeta `out`:

```
sudo mkdir out
```

4. Y ahora, comenzamos la construcción de la ISO con:

```
sudo ./build.sh -v
```

Una vez finalizado sin errores, la ISO generada estará dentro de la carpeta `out`.

Si quisiéramos regenerar la ISO, al volver a ejecutar el paso `4`, automáticamente se eliminarán los contenidos de las carpetas de trabajo, y de generación del instalador.

En el caso de que hayamos copiado la ISO a alguna otra parte de nuestro computador, podremos eliminar esta carpeta:

```
sudo rm -r cnchi-gnome-based
```

<br>

### Un poco de información

- `build.sh`: es el script utilizado para construir la ISO del instalador. 
- **`HELP_ME.sh`: script para construir y actualizar la ISO fácilmente. ¡No se necesitan conocimientos avanzados!**
- `packages.both`: contiene un listado de paquetes a ser instalados en la ISO durante su generación. Para cambiar el escritorio, el login manager, o cualquier otro paquete, simplemente edite este archivo.
- `pacman.conf`: es el archivo pacman.conf utilizado por build.sh para generar la ISO. Al utilizar éste, se ignorará cualquier configuración personal existente en su sistema (en `/etc/pacman.conf`).
- `run.sh`: Archivo inútil Voy a deshacerme de él pronto.
- `TEST_FILE.sh`: ¿Adivina qué? Otro archivo obsoleto e inútil a la espera de ser eliminado.
- `translations.sh`: ¡Traducción mágica!
- `pacman-init.service`: Agrega un módulo pacman a systemd. Creo que funciona en nuestro ISO, pero honestamente es algo enganchado con el ISO de Antergos, y no sé del todo si hace algo en el nuestro. Sin embargo, seguro que suena como un archivo útil, ¿verdad?
- `clean.sh`: Archivo fácil de usar para eliminar su carpeta `build` y` out`, en general solo un método fácil para tener todo limpio y listo para construir otro ISO.
- `config`: Configuraciones para qué versión de Cnchi usar en nuestro ISO, así como de dónde obtenerla.
- `gitv2.sh`: Un archivo especial hecho por Palanthis para automatizar los cambios de desarrollo de empuje hasta Gitlab aquí.

<br>

Pueden acceder a la página de RebornOS <a href="https://rebornos.org/" class="button">haciendo clic aquí</a>.

<br>
<br>
<br>
<br>


# Comment construire l'installateur ISO de RebornOS

La dernière version stable est: RebornOS-2020.02.08-x86_64.iso

## Lieux de téléchargement de l'installateur : ##

Sur le site Web de <a href="https://rebornos.org/" class="button">RebornOS</a>

Depuis notre page de téléchargement sur <a href="https://osdn.net/projects/rebornos/releases" class="button">OSDN</a>

Depuis notre page de téléchargement sur <a href="https://sourceforge.net/projects/rebornos/files/" class="button">Sourceforge</a>

À partir de notre <a href="https://repo.rebornos.org/RebornOS/iso/" class="button">Serveur</a>

<br>

### Dépendances

- isolinux/syslinux
- arch-install-scripts
- cpio
- dosfstools
- git 
- libisoburn
- mkinitcpio-nfs-utils
- make
- opendesktop-fonts
- patch
- squashfs-tools
- archiso
- lynx
- wget
- yad

<br>

### Étapes à suivre pour créer le programme d'installation

1. Nous devrons installer les dépendances:

```
sudo pacman -S arch-install-scripts cpio dosfstools git libisoburn mkinitcpio-nfs-utils make patch squashfs-tools wget lynx archiso yad --noconfirm --needed
```
Une fois installé, nous redémarrons le système.

2. Ensuite, cloner le référentiel localement, et l'entrer (un dossier nommé `cnchi-gnome-based` sera créé). Ceci est fait avec :

```
git clone https://gitlab.com/reborn-os-team/cnchi-gnome-based.git --recursive && cd cnchi-gnome-based
```

3. Créez un dossier `out`:

```
sudo mkdir out
```

Et maintenant, nous commençons la construction de l'ISO avec:

```
sudo ./build.sh -v
```

Une fois terminé sans erreur, l'ISO généré sera dans le dossier `out`.

Si nous voulons régénérer l'ISO, en exécutant à nouveau l'étape `4`, le contenu des dossiers de travail et des dossiers de génération de l'installateur sera automatiquement supprimé.

Si nous avons copié l'ISO dans une autre partie de notre ordinateur, nous pourrons supprimer ce dossier :

```
sudo rm -r cnchi-gnome-based
```

### Un peu d'information:

- `build.sh`: est le script utilisé pour construire l'ISO de l'installateur. 
- **`HELP_ME.sh`: script pour construire et mettre à jour facilement l'ISO. Aucune connaissance avancée n'est nécessaire!**
- `packages.both`: contient une liste de paquets à installer dans l'ISO pendant sa génération. Pour changer le bureau, le gestionnaire de connexion ou tout autre paquet, éditez simplement ce fichier.
- `pacman.conf`: est le fichier pacman.conf utilisé par build.sh pour générer l'ISO. Lors de son utilisation, toute configuration personnelle existante dans votre système (dans `/etc/pacman.conf`) sera ignorée.
- `run.sh`: Je vais bientôt m'en débarrasser.
- `TEST_FILE.sh`: Devinez quoi ? Un autre fichier obsolète et inutile qui attend d'être supprimé.
- `translations.sh`: Traduction magique!
- `pacman-init.service`: Ajouter un module pacman à systemd. Je pense que cela fonctionne sur notre ISO, mais honnêtement, c'est un peu accro à l'Ancien ISO, et je ne sais pas si cela fait quoi que ce soit sur le nôtre. Cependant, cela sonne certainement comme un fichier utile, n'est-ce pas?
- `clean.sh`: Fichier facile à utiliser pour supprimer votre dossier `build' et `out', en général une méthode simple pour avoir tout propre et prêt à construire un autre ISO.
- `config`: Configurations pour quelle version de Cnchi utiliser dans notre ISO, ainsi que où l'obtenir.
- `gitv2.sh`: Un fichier spécial réalisé par Palanthis pour automatiser les changements de développement push jusqu'à Gitlab ici.

<br>

Vous pouvez accéder à la page RebornOS <a href="https://rebornos.org/" class="button">en cliquant ici</a>.


