#  Copyright © 2016-2019 RebornOS
#
#  This file is part of RebornOS.
#
#  Reborn OS is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  Reborn OS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  The following additional terms are in effect as per Section 7 of the license:
#
#  The preservation of all legal notices and author attributions in
#  the material or in the Appropriate Legal Notices displayed
#  by works containing it is required.
#
#  You should have received a copy of the GNU General Public License
#  along with Reborn OS; If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash
##############################
# Script to download and install Cnchi #
##############################


# Defining Variables
export CNCHI_GIT_BRANCH="0.16.21"
export CNCHI_GIT_URL="https://github.com/Antergos/Cnchi/archive/${CNCHI_GIT_BRANCH}.zip"
export script_path="/usr/share"
export REBORN="/usr/share/cnchi/reborn"

QUESTION(){
echo
echo "Please select your preferred course of action:"
echo
options=("Remove all instances of Cnchi from my system" "Build Cnchi" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Remove all instances of Cnchi from my system")
            REMOVE;break;;
        "Build Cnchi")
            RUN;break;;
        "Quit")
            break;;
        *) echo "ERROR!!! ERROR!!!! SOUND THE ALARM!!!"
            echo "Sadly, option $REPLY is not possible! Please select either option 1, 2, or 3 instead. Thank you!";;
    esac
done
}

# Removing Cnchi files if they exist
REMOVE(){
echo
echo "REMOVING ALL INSTALLED INSTANCES OF CNCHI..."
if [ -f /usr/share/cnchi/bin/cnchi ]; then
rm -rf /usr/share/cnchi
rm -rf /var/log/cnchi
rm -f /usr/bin/cnchi
rm -f /usr/bin/cnchi-start.sh
rm -f /usr/share/applications/antergos-install.desktop
rm -f /usr/share/applications/cnchi.desktop
rm -f /usr/share/pixmaps/cnchi.png
fi
echo "DONE"
}
# Downloading and installing Cnchi
INSTALL(){
echo
echo "#########################################################"
echo "########## DOWNLOADING & INSTALLING CNCHI... ############"
echo "#########################################################"
echo
    wget "${CNCHI_GIT_URL}" -O ${script_path}/cnchi-git.zip
    unzip ${script_path}/cnchi-git.zip -d ${script_path}
    rm -f ${script_path}/cnchi-git.zip
    CNCHI_SRC="${script_path}/Cnchi-${CNCHI_GIT_BRANCH}"
        install -d ${script_path}/{cnchi,locale}
	install -Dm755 "${CNCHI_SRC}/bin/cnchi" "/usr/bin/cnchi"
         echo
         echo "COPIED STARTUP FILE OVER"
         echo
	install -Dm755 "${CNCHI_SRC}/cnchi.desktop" "/usr/share/applications/cnchi.desktop"
         echo
         echo "COPIED DESKTOP FILE OVER"
         echo
	install -Dm644 "${CNCHI_SRC}/data/images/antergos/antergos-icon.png" "/usr/share/pixmaps/cnchi.png"
         echo
         echo "COPIED CNCHI ICON OVER"
         echo
    # TODO: This should be included in Cnchi's src code as a separate file
    # (as both files are needed to run cnchi)
    sed -r -i 's|\/usr.+ -v|pkexec /usr/share/cnchi/bin/cnchi -s bugsnag|g' "/usr/bin/cnchi"
    echo
    echo "MODIFIED STARTUP COMMAND FOR CNCHI"
    echo
    for i in ${CNCHI_SRC}/src ${CNCHI_SRC}/bin ${CNCHI_SRC}/data ${CNCHI_SRC}/scripts ${CNCHI_SRC}/ui; do
        cp -R ${i} "${script_path}/cnchi/"
        echo
        echo "COPIED MAIN CNCHI'S SUBDIRECTORIES OVER TO BUILD FOLDER"
        echo
    done
    for files in ${CNCHI_SRC}/po/*; do
        if [ -f "$files" ] && [ "$files" != 'po/cnchi.pot' ]; then
            STRING_PO=`echo ${files#*/}`
            STRING=`echo ${STRING_PO%.po}`
            mkdir -p /usr/share/locale/${STRING}/LC_MESSAGES
            msgfmt $files -o /usr/share/locale/${STRING}/LC_MESSAGES/cnchi.mo
            echo "${STRING} installed..."
            echo "CNCHI IS NOW BUILT"
        fi
    done
rm -rf ${script_path}/Cnchi-${CNCHI_GIT_BRANCH}
}

# Ask whether or not to use experimental files for Cnchi
ASK(){
echo
echo
echo "Please select your preferred course of action:"
echo
options=("Use our official Cnchi files" "Do not use modified Cnchi files" "Use your own files instead" "Setup System for Reborn installation" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Use our official Cnchi files")
            DOWNLOAD_SIMPLE;break;;
        "Do not use modified Cnchi files")
            ANTERGOS;break;;
        "Use your own files instead")
            FILES;break;;
        "Setup System for Reborn installation")
            DOWNLOAD_ADVANCED;break;;
        "Quit")
            break
            ;;
        *) echo "ERROR!!! ERROR!!!! SOUND THE ALARM!!!"
            echo "Sadly, option $REPLY is not possible! Please select either option 1, 2, 3, 4, or 5 instead. Thank you!";;
    esac
done
}

# Use plain old Antergos files (useful for testing sometimes)

ANTERGOS(){
echo
echo
echo "Nice choice! I know I use this one sometimes to test things myself as well, when I need more control with files"
echo "Just remember - have fun and continue on! Your work truly means a lot to us at Reborn OS."
echo
echo "DOWNLOADING A FEW FILES TO HELP CNCHI START PROPERLY FOR YOU..."
echo
echo
cd /
cd /usr/share/cnchi/
mkdir $REBORN
cd $REBORN/
git clone https://gitlab.com/RebornOS/RebornOS.git --recursive
cd /
cd /usr/bin/
if [ -f /usr/bin/cnchi-start.sh ]; then
rm -f /usr/bin/cnchi-start.sh
fi
wget https://gitlab.com/RebornOS/RebornOS/raw/master/airootfs/usr/bin/cnchi-start.sh
chmod +x /usr/bin/cnchi-start.sh
cd /
cd /usr/share/applications/
if [ -f /usr/share/applications/cnchi.desktop ]; then
rm -f /usr/share/applications/cnchi.desktop
fi
if [ -f /usr/share/applications/antergos-install.desktop ]; then
rm -f /usr/share/applications/antergos-install.desktop
fi
cp $REBORN/RebornOS/airootfs/usr/share/applications/antergos-install.desktop /usr/share/applications/
echo
echo "DONE WITH EVERYTHING!"
echo "ENJOY ANTERGOS' CNCHI"
}

# Use your own files for Cnchi
FILES(){
echo
echo
echo "Fabulous! I see you're quite adventurous' :)"
echo "Well, have fun and happy hacking."
yad --center --width=350 --height=100 --form --separator='' --title="Select" --text="Select Disired Directory" --save --field="":CDIR > /tmp/reborn-directory.txt
SAVING=$(sed '1q;d' /tmp/reborn-directory.txt)
cd $SAVING
echo
echo "DOWNLOADING SPECIAL IMAGE FILES FOR CNCHI. THEN INSTALLING YOUR FILES AS WELL..."
echo
echo
cd /
cd /usr/share/cnchi/
mkdir $REBORN
cd $REBORN/
git clone https://gitlab.com/RebornOS/RebornOS.git --recursive
cp -r $SAVING $REBORN/Cnchi
mv $REBORN/RebornOS/images $REBORN/images
cd /
cd /usr/bin/
if [ -f /usr/bin/cnchi-start.sh ]; then
rm -f /usr/bin/cnchi-start.sh
fi
wget https://gitlab.com/RebornOS/RebornOS/raw/master/airootfs/usr/bin/cnchi-start.sh
chmod +x /usr/bin/cnchi-start.sh
cd /
cd /usr/share/applications/
if [ -f /usr/share/applications/cnchi.desktop ]; then
rm -f /usr/share/applications/cnchi.desktop
fi
if [ -f /usr/share/applications/antergos-install.desktop ]; then
rm -f /usr/share/applications/antergos-install.desktop
fi
cp $REBORN/RebornOS/airootfs/usr/share/applications/antergos-install.desktop /usr/share/applications/
echo "DONE"
rm -f /tmp/reborn-directory.txt
CUSTOMIZE
}

# Download Reborn's normal Cnchi files
DOWNLOAD_SIMPLE(){
echo
echo
echo "Fabulous! Checking out Reborn I see :)"
echo "Well, have fun and happy hacking."
echo
echo "DOWNLOADING SPECIAL REBORN FILES FOR CNCHI..."
echo
echo
cd /
cd /usr/share/cnchi/
mkdir $REBORN
cd $REBORN/
git clone https://gitlab.com/RebornOS/RebornOS.git --recursive
mv $REBORN/RebornOS/Cnchi $REBORN/Cnchi
mv $REBORN/RebornOS/images $REBORN/images
cd /
cd /usr/bin/
if [ -f /usr/bin/cnchi-start.sh ]; then
rm -f /usr/bin/cnchi-start.sh
fi
wget https://gitlab.com/RebornOS/RebornOS/raw/master/airootfs/usr/bin/cnchi-start.sh
chmod +x /usr/bin/cnchi-start.sh
cd /
cd /usr/share/applications/
if [ -f /usr/share/applications/cnchi.desktop ]; then
rm -f /usr/share/applications/cnchi.desktop
fi
if [ -f /usr/share/applications/antergos-install.desktop ]; then
rm -f /usr/share/applications/antergos-install.desktop
fi
cp $REBORN/RebornOS/airootfs/usr/share/applications/antergos-install.desktop /usr/share/applications/
echo "DONE"
CUSTOMIZE
}

# Install REBORN
DONWLOAD_ADVANCED() {
  echo
  echo
  echo "Fabulous! Welcome to Reborn :)"
  echo "Well, have fun and happy hacking."
  echo
  echo "DOWNLOADING SPECIAL REBORN FILES FOR CNCHI..."
  echo
  echo
  cd /
  cd /usr/share/cnchi/
  mkdir $REBORN
  cd $REBORN/
  git clone https://gitlab.com/RebornOS/RebornOS.git --recursive
  mv $REBORN/RebornOS/Cnchi $REBORN/Cnchi
  mv $REBORN/RebornOS/images $REBORN/images
  cd /
  cd /usr/bin/
  if [ -f /usr/bin/cnchi-start.sh ]; then
  rm -f /usr/bin/cnchi-start.sh
  fi
  wget https://gitlab.com/RebornOS/RebornOS/raw/master/airootfs/usr/bin/cnchi-start.sh
  chmod +x /usr/bin/cnchi-start.sh
  cd /
  cd /usr/share/applications/
  if [ -f /usr/share/applications/cnchi.desktop ]; then
  rm -f /usr/share/applications/cnchi.desktop
  fi
  if [ -f /usr/share/applications/antergos-install.desktop ]; then
  rm -f /usr/share/applications/antergos-install.desktop
  fi
  cp $REBORN/RebornOS/airootfs/usr/share/applications/antergos-install.desktop /usr/share/applications/
  #Use lightdm.conf from local direcectory instead of default one
  echo "Removing unnecessary lightdm.conf"
  rm /etc/lightdm/lightdm.conf
  echo "Copying correct lightdm.conf file over"
  cp $REBORN/airootfs/etc/lightdm/lightdm.conf /etc/lightdm/
  echo "DONE"
  echo
  #Use sddm.conf from local direcectory instead of default one
  echo "Removing unnecessary sddm.conf"
  rm /etc/sddm.conf
  echo "Copying correct sddm.conf file over"
  cp $REBORN/etc/sddm.conf /etc/
  echo "DONE"
  echo
  #Copy Antergos Mirrorlist
  echo "Setting up Antergos and Reborn Mirrorlist"
  mkdir -p /etc/pacman.d
  cp $REBORN/airootfs/etc/antergos-mirrorlist /etc/pacman.d/
  cp $REBORN/airootfs/etc/reborn-mirrorlist /etc/pacman.d/
  echo "DONE"
  echo
  #Copy pacman-init.service over
  echo "Copying pacman-init.service"
  cp $REBORN/pacman-init.service /etc/systemd/system/
  echo "DONE"
  echo
  #Replace pacman.conf with Reborn's
  echo "Replacing pacman.conf with Reborn's"
  rm /etc/pacman.conf
  cp $REBORN/Cnchi/pacman.conf /airootfs/etc/
  cp $REBORN/Cnchi/pacman.conf /usr/share/cnchi/
  cp $REBORN/Cnchi/pacman2.conf /
  mv /pacman2.conf /tmp/pacman.conf
  echo "DONE"
  echo
  CUSTOMIZE
}

# Customize Cnchi for Reborn OS
CUSTOMIZE(){
echo
echo
echo "MOVING DOWNLOADED FILES OVER..."
echo
echo

#Editting Cnchi
echo "Moving Cnchi files over..."

cp $REBORN/Cnchi/packages.xml /usr/share/cnchi/data/
cp $REBORN/Cnchi/pacman.tmpl /usr/share/cnchi/data/
cp $REBORN/Cnchi/features_info.py /usr/share/cnchi/src/
cp $REBORN/Cnchi/features.py /usr/share/cnchi/src/pages/
cp $REBORN/Cnchi/desktop_info.py /usr/share/cnchi/src/
cp $REBORN/Cnchi/grub2.py /usr/share/cnchi/src/installation/boot/
cp $REBORN/Cnchi/10_antergos /usr/share/cnchi/scripts/
cp $REBORN/Cnchi/systemd_boot.py /usr/share/cnchi/src/installation/boot/
cp $REBORN/Cnchi/postinstall.sh /usr/share/cnchi/scripts/
cp $REBORN/Cnchi/info.py /usr/share/cnchi/src/
cp $REBORN/Cnchi/show_message.py /usr/share/cnchi/src/
cp $REBORN/Cnchi/slides.py /usr/share/cnchi/src/pages/
cp $REBORN/Cnchi/check.py /usr/share/cnchi/src/pages/
cp $REBORN/Cnchi/welcome.py /usr/share/cnchi/src/pages/
cp $REBORN/Cnchi/antergos-logo-mini2.png /usr/share/cnchi/data/images/antergos/
cp $REBORN/Cnchi/20-intel.conf /usr/share/cnchi/
cp $REBORN/Cnchi/lightdm-webkit2-greeter.conf /usr/share/cnchi/
cp $REBORN/Cnchi/1.png /usr/share/cnchi/data/images/slides/
cp $REBORN/Cnchi/2.png /usr/share/cnchi/data/images/slides/
cp $REBORN/Cnchi/3.png /usr/share/cnchi/data/images/slides/
cp $REBORN/Cnchi/sddm.conf /usr/share/cnchi/
cp $REBORN/airootfs/usr/share/applications/cnchi.png /usr/share/pixmaps/
cp $REBORN/Cnchi/antergos-icon.png /usr/share/cnchi/data/images/antergos/antergos-icon.png
cp $REBORN/scripts/tint2-start.desktop /usr/share/cnchi/
cp $REBORN/scripts/conky-start.desktop /usr/share/cnchi/
cp $REBORN/scripts/obmenu-gen.desktop /usr/share/cnchi/
cp $REBORN/scripts/plymouth-reborn.desktop /usr/share/cnchi/
cp $REBORN/scripts/mate-panel.desktop /usr/share/cnchi/
cp $REBORN/scripts/plymouth.sh /usr/share/cnchi/
cp $REBORN/scripts/flatpak.sh /usr/share/cnchi/
cp $REBORN/scripts/pkcon.sh /usr/share/cnchi/
cp $REBORN/scripts/pkcon2.sh /usr/share/cnchi/
cp $REBORN/scripts/flatpak.desktop /usr/share/cnchi/
cp $REBORN/scripts/update.desktop /usr/share/cnchi/
cp $REBORN/scripts/openbox-config.sh /usr/share/cnchi/
cp $REBORN/scripts/deepin-fix.sh /usr/share/cnchi/
cp $REBORN/scripts/deepin-fix.service /usr/share/cnchi/
cp $REBORN/images/pantheon.png /usr/share/cnchi/data/images/desktops/
cp $REBORN/images/apricity.png /usr/share/cnchi/data/images/desktops/
cp $REBORN/images/deepin.png /usr/share/cnchi/data/images/desktops/
cp $REBORN/images/cinnamon.png /usr/share/cnchi/data/images/desktops/
cp $REBORN/images/windows.png /usr/share/cnchi/data/images/desktops/
cp $REBORN/images/kde.png /usr/share/cnchi/data/images/desktops/
cp $REBORN/images/i3.png /usr/share/cnchi/data/images/desktops/
cp $REBORN/images/lxqt.png /usr/share/cnchi/data/images/desktops/
cp $REBORN/images/openbox.png /usr/share/cnchi/data/images/desktops/
cp $REBORN/images/enlightenment.png /usr/share/cnchi/data/images/desktops/
cp $REBORN/images/xfce.png /usr/share/cnchi/data/images/desktops/
cp $REBORN/images/desktop-environment-apricity.svg /usr/share/cnchi/data/icons/scalable/
cp $REBORN/images/desktop-environment-pantheon.svg /usr/share/cnchi/data/icons/scalable/
cp $REBORN/images/desktop-environment-windows.svg /usr/share/cnchi/data/icons/scalable/
cp $REBORN/images/desktop-environment-budgie.svg /usr/share/cnchi/data/icons/scalable/
cp $REBORN/images/desktop-environment-i3.svg /usr/share/cnchi/data/icons/scalable/
cp $REBORN/Cnchi/reborn-mirrorlist /etc/pacman.d/
echo "DONE"
echo
echo "Replacing Antergos mentions with Reborn"
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/advanced.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/alongside.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/ask.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/automatic.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/check.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/gtkbasebox.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/keymap.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/language.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/location.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/installation/process.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/slides.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/summary.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/timezone.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/user_info.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/wireless.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/zfs.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/pages/desktop.py
sed -i "s/gnome/deepin/g" /usr/share/cnchi/src/pages/desktop.py
#sed -i "s/Antergos/Reborn/g" ${work_dir}/${arch}/airootfs/usr/share/cnchi/src/encfs.py
sed -i "s/Antergos/Reborn/g" /usr/share/cnchi/src/main_window.py
echo "DONE"
}

RUN(){
echo
echo
echo "YAY! Thank you for your help in maintaining Reborn."
echo "We surely need it! So have fun, and feel free to"
echo "message me (Keegan) anytime you want. Got questions?"
echo "Just ask! Thanks, and good luck."
echo
echo
echo
REMOVE
INSTALL
ASK
}

export -f QUESTION DOWNLOAD_SIMPLE DOWNLOAD_ADVANCED ASK CUSTOMIZE INSTALL REMOVE RUN ANTERGOS

QUESTION
